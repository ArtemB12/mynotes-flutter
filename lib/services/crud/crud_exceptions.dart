class DatabaseAlreadyOpenException implements Exception {}

class UnableTogGetDocumentsDirectory implements Exception {}

class DatabaseIsNotOpen implements Exception {}

class CouldNotDeleteUser implements Exception {}

class UserAlreadyExists implements Exception {}

class CouldNotFindUser implements Exception {}

class CouldNotDeleteNote implements Exception {}

class CouldNotFindNotes implements Exception {}

class CouldNotUpdateNote implements Exception {}
